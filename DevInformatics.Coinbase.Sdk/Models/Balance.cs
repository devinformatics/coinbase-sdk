﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace DevInformatics.Coinbase.Sdk.Models
{
    public class Balance
    {
        [JsonProperty(PropertyName="amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName="currency")]
        public Currency Currency { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
