﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInformatics.Coinbase.Sdk.Models
{
    public class Address
    {
        [JsonProperty(PropertyName="address")]
        public CoinbaseAddress CoinbaseAddress { get; set; }
    }
}
