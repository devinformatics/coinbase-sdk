﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInformatics.Coinbase.Sdk.Models
{
    public class CoinbaseAddressesResults
    {
        public CoinbaseAddressesResults()
        {
            Addresses = new List<Address>();
        }

        [JsonProperty("addresses")]
        public IEnumerable<Address> Addresses { get; set; }
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
        [JsonProperty("num_pages")]
        public int PageCount { get; set; }
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }
    }
}
