﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInformatics.Coinbase.Sdk.Models
{
    public class CoinbaseAddress
    {
        [JsonProperty(PropertyName="address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName="callback_url")]
        public string CallbackUrl { get; set; }
        [JsonProperty(PropertyName="label")]
        public string Label { get; set; }
        [JsonProperty(PropertyName="created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
