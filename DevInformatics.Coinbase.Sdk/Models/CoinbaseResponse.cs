﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevInformatics.Coinbase.Sdk.Models
{
    internal class CoinbaseResponse
    {
        [JsonProperty(PropertyName="success")]
        public bool Success { get; set; }
        [JsonProperty(PropertyName="address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName="callback_url")]
        public string CallbackUrl { get; set; }
    }
}
