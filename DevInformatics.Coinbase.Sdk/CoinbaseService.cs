﻿using DevInformatics.Coinbase.Sdk.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DevInformatics.Coinbase.Sdk
{
    public class CoinbaseService
    {
        private const string CoinbaseURI = "https://coinbase.com/api/v1";

        public string ApiKey { get; set; }

        public CoinbaseService()
        {

        }

        public CoinbaseService(string apiKey)
        {
            ApiKey = apiKey;
        }

        public Balance GetBalance()
        {
            HttpWebRequest webRequest = HttpWebRequest.CreateHttp(CoinbaseURI + "/account/balance?api_key=" + ApiKey);
            Balance balance = null;
            using(StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                balance = JsonConvert.DeserializeObject<Balance>(reader.ReadToEnd());
            } // using
            return balance;
        }

        public string GetReceiveAddress()
        {
            HttpWebRequest webRequest = HttpWebRequest.CreateHttp(CoinbaseURI + "/account/receive_address?api_key=" + ApiKey);
            string address = string.Empty;
            using(StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                CoinbaseResponse coinbaseResponse = JsonConvert.DeserializeObject<CoinbaseResponse>(reader.ReadToEnd());
                address = coinbaseResponse.Address;
            } // using
            return address;
        }

        public CoinbaseAddressesResults GetAddresses(int page = 1, int limit = 25)
        {
            HttpWebRequest webRequest = HttpWebRequest.CreateHttp(CoinbaseURI + string.Format("/addresses?page={0}&limit={1}&api_key={2}", page, limit, ApiKey));
            CoinbaseAddressesResults results;
            using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                results = JsonConvert.DeserializeObject<CoinbaseAddressesResults>(reader.ReadToEnd());
            } // using
            return results;
        }

        public IEnumerable<CoinbaseUser> GetUsers()
        {
            HttpWebRequest webRequest = HttpWebRequest.CreateHttp(CoinbaseURI + string.Format("/users?&api_key={0}", ApiKey));
            CoinbaseUserResults results;
            using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                results = JsonConvert.DeserializeObject<CoinbaseUserResults>(reader.ReadToEnd());
            } // using
            return results.UserContainers.Select(uc => uc.User);
        }
    }
}
