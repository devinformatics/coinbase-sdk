﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevInformatics.Coinbase.Sdk;
using Devinformatics.Coinbase.ConsoleDemo;
using DevInformatics.Coinbase.Sdk.Models;

namespace DevInformatics.Coinbase.ConsoleDemo
{
    class Program
    {
        private static readonly Configuration configuration = new Configuration();

        static void Main(string[] args)
        {
            CoinbaseService service = new CoinbaseService(configuration.ApiKey);
            Balance balance = service.GetBalance();
            Console.WriteLine(balance);
            Console.WriteLine(service.GetReceiveAddress());
            Console.WriteLine(service.GetAddresses());
            Console.WriteLine(service.GetUsers());
            Console.ReadKey();
        }
    }
}
